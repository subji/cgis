/**
 * 이 프로그램은 Biobank로부터 Sample WTS 분석결과(WTS_RESULTS_TB)를 읽어 저장한다.
 * @constructor J.H.Kim
 */

var https = require('https');
var http = require('http');
var fs = require('fs');
var async = require('async');
var moment = require('moment');
var config = require('../../../config.json');
var getConnection = require('../../../routes/modules/mysql_connection');
//TODO Self Signed Error 때문에 설정함. 공인 CA를 통한 https 요청시에는 불필요함.
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

/**
 * 해당 Sample의 results를 서버로부터 읽어와 Database에 입력한다.
 * @param {string} sample_id - Sample ID
 * @param function done - 작업 완료 후 호출할 Callback
 */
var insert_results = function(sample_id, done) {
  // CSV를 읽어올 URL
  var url = config.biobank.url.https + '/openapi/sample/wts?sample_id=' + sample_id;

  //저장할 임시 파일
  var filename = "/tmp/" + "wts_" + new Date().getTime() + ".csv"; // Unix Millisecond Timestamp;
  var file = fs.createWriteStream(filename);

  https.get(url, function(res) {
    if (res.statusCode !== 200)
      done(new Error('Http Request Error:' + res.statusCode + ':' + res.statusMessage + ',url:' + url));
    res.pipe(file);
    file.on('finish', function() {
      file.close(function() { // 파일 저장 완료 후 파일을 DB에 로드함.
        load_data_from_csvfile(filename, done);
      });
    });
  }).on('error', function(err) {
    done(err);
  });
};

/**
 * CSV 파일로부터 GS_WTS_RESULTS_TB로 대량으로 데이터 입력
 * 참고: 6800rows 입력하는데 insert는 38분소요, load data infile은 4초 소요.
 * @param  {[string]}   filename [업로드할 csv file 이름]
 * @param  {Function} done     [작업 완료 후 호출할 Callback]
 */
var load_data_from_csvfile = function(filename, done) {
  console.log("\t load data file from %s", filename);
  var sql = "load data infile ? into table CGIS.GS_WTS_RESULTS_TB fields terminated by ',' enclosed by '\"' lines terminated by '\r\n'";
  getConnection(function(connection) {
    connection.query(sql, [filename], function(err, result) {
      if (!err) //TODO result.warningCount > 0 경우도 Error 처리 
        console.log("\t insert %d results(message: %s)", result.affectedRows, result.message);
      done(err);
    });
  });
};

/**
 * 해당 Sample의 results 를 Database에서 삭제한다.
 * @param {string} sample_id - Sample ID
 * @param function done - 작업 완료 후 호출할 Callback
 */
var delete_results = function(sample_id, done) {
  getConnection(function(connection) {
    connection.query('delete from GS_WTS_RESULTS_TB where sample_id = ?', [sample_id], function(err, result) {
      if (!err)
        console.log("\t delete %d results.", result.affectedRows);
      done(err);
    });
  });
};

/**
 * 해당 Sample 동기화 작업이 완료되었음을 서버에 알린다. 서버는 wts_download_flag를 1로 바꾼다.
 * @param {string} sample_id - Sample ID
 * @param function done - 작업 완료 후 호출할 Callback
 */
var notify_wts_sync_complete = function(sample_id, done) {
  var url = config.biobank.url.https + '/openapi/sample/wts_sync_complete?sample_id=' + sample_id;
  // console.log("\t url", url);

  https.get(url, function(res) {
    var data = [];
    res.on('data', function(chunk) {
      data.push(chunk);
    }).on('end', function() {
      var buffer = Buffer.concat(data);
      var str = buffer.toString('utf8');
      var result;
      try {
        result = JSON.parse(str);
      } catch (err) {
        done(err);
      }
      console.log("\t notify_wts_sync_complete", result.message);
      if (0 === result.code)
        done();
      else
        done(new Error(result.message));
    });
  }).on('error', function(err) {
    done(err);
  });

};

/**
 * wts 분석이 완료된 Sample의 Results를 서버와 동기화하고, 동기화가 완료되면 이를 서버에 알린다.
 * @param {string} sample_id - Sample ID
 * @param function finish - 작업 완료 후 호출할 Callback
 */
module.exports = function(sample_id, finish) {
  if (!sample_id)
    finish(new Error('No Sample Id'));

  // Main Logic
  async.waterfall([
    // 1. delete old results
    function(done) {
      delete_results(sample_id, done);
    },
    // 2. insert results
    function(done) {
      insert_results(sample_id, done);
    },
    // 3. notify to biobank that sync completed
    function(done) {
      notify_wts_sync_complete(sample_id, done);
    }
  ], function(err, results) {
    finish(err);
  });
};
