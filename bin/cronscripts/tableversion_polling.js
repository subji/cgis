#!/usr/bin/env node

/**
 * 이 프로그램은 Biobank로부터 Table Version을 읽어 CGIS의 버전과 비교하여 최신의 것으로 적용한다.
 * cron이 이 프로그램을 실행한다.
 * cron 확인은 crontab -e
 * 로그는 /tmp/crontab.log에 저장한다.
 * @constructor J.H.Kim
 */

var https = require('https');
var moment = require('moment');
var getConnection = require('../../routes/modules/mysql_connection');
var async = require('async');
var multiline = require('multiline');
var config = require('../../config.json');
var sync_table = require('./modules/sync_table');
var url = config.biobank.url.https + '/openapi/tableversion';
//TODO Self Signed Error 때문에 설정함. 공인 CA를 통한 https 요청시에는 불필요함.
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

var title = 'Table Version Polling';

var version_query = 'select table_name name,table_version ver from BIOBANK.PD_VERSION_TB where table_name = ?';
var version_update = 'update BIOBANK.PD_VERSION_TB set table_version = ? where table_name = ?';

var end_message = function(type, msg) {
  if (type) console.log(type, msg);
  console.log('-------------   End at %s (%s)', moment().format('MM/DD HH:mm:ss'), title);
};

console.log('\n------------- Start at %s (%s)', moment().format('MM/DD HH:mm:ss'), title);
https.get(url, function(res) {
  var data = [];
  res.on('data', function(chunk) {
    data.push(chunk);
  }).on('end', function() {
    var buffer = Buffer.concat(data);
    var str = buffer.toString('utf8');
    var biobank_tables;
    try {
      biobank_tables = JSON.parse(str);
    } catch (err) {
      end_message('Error', err);
      process.exit();
    }
    getConnection(function(connection) {
      console.log('# of biobank_tables : %d', biobank_tables.length);
      async.eachSeries(biobank_tables, function(biobank_table, done) {
        connection.query(version_query, [biobank_table.name], function(err, result) {
          var cgis_table = result[0];
          // cgis에 테이블이 존재하여야 한다.
          if (cgis_table !== undefined) {
            if (cgis_table !== undefined && biobank_table.ver !== cgis_table.ver) {
              console.log('%s: BIOBANK version(%s), CGIS version(%s)', biobank_table.name, biobank_table.ver, cgis_table.ver);
              sync_table(biobank_table, cgis_table, done);
            } else {
              done();
            }
          } else {
            done();
          }
        });
      }, function(err) {
        if (err)
          console.log('Error:', err);
        end_message();
        process.exit();
      });
    });
  });
}).on('error', function(err) {
  end_message('Https Error', err);
  process.exit();
});
