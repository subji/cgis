//var express = require('express');
var request = require('supertest');
var assert = require('assert');
var config = require('../config.json');

describe('시스템 연결 테스트', function() {
  // Certi 없이 https 테스트하기 위함.
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

  it('BioCloud http(80)으로 연결할 수 있어야 한다.', function(done) {
    var host = config.biobank.url.http;
    request(host)
      .get('/')
      .expect(200, done);
  });

  it('BioCloud https(443)으로 연결할 수 있어야 한다.', function(done) {
    var host = config.biobank.url.https;
    request(host)
      .get('/openapi/analysis?institute_id=' + config.institute_id)
      .expect(200, done);
  });

  it('Galaxy에서 BAM 파일 다운로드할 수 있어야 한다.', function(done) {
    var host = config.galaxy.url.http;
    // 개발 갤럭시, 실 갤럭시 둘 다 이승재 차장이 해당 파일 생성하였음.2016.09.23
    // 개발 갤럭시 재설치하여 김동인 과장이 파일 새로 생성.2017.05.24
    var file = 'test.txt';
    request(host)
      .get('/download/bam/' + file)
      .expect(200, done);
  });

});
