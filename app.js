var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
// var compression = require('compression');
var config = require('./config.json');

// Helmet helps you secure your Express apps by setting various HTTP headers.
// It's not a silver bullet, but it can help!
var helmet = require('helmet');
// Passport
var passport = require('passport');
var flash = require('connect-flash'); //Message 전달
require('./routes/modules/passport-mysql-local')(passport);

// Security
var security = require('./routes/modules/security');

//Session정보 저장
var session = require('express-session');
var MongoStore = require('connect-mongo')(session); //Session정보 저장
var options = require('./routes/modules/mongo-session-options.js'); //MongoDB Session Collection Information

var root = require('./routes/index');
//var admin = require('./routes/admin');
var menu = require('./routes/menu');

// Chart view
var chart = require('./routes/chart');
// Chart RESTful
var rest = require('./routes/rest');
var rest_biobank = require('./routes/rest_biobank');
// File Upload,  TODO 이부분 쓰는지 확인, 만약 안 쓰면 관련 모듈 삭제 요망.
var files = require('./routes/files');

// User Restful
var users = require('./routes/models/users');
// Sample
var sample = require('./routes/models/sample');
// cohort
var cohort = require('./routes/models/cohort');
// Drug Restful
var drug = require('./routes/models/drug');

var app = express();

// Global Variables
app.locals.title = config.title;
app.locals.sub_brand = config.sub_brand;
app.locals.community = config.community;
app.locals.biobank_url = config.biobank.url.http;

app.use(helmet());
// Session Management
// 여기서는 설정하지 않았지만, Secure Session에 대해서는 다음을 참조하라: https://gist.github.com/nikmartin/5902176
app.use(session({
    secret: 'keyboard cat',
    resave: true, //default
    saveUninitialized: true, //default
    store: new MongoStore(options),
    cookie: {
        httpOnly: true, // for sso.
    }
}));

// Passport
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/images/favicon.png'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// For contents gzip compression --> nginx
// app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());

// static --> nginx
//app.use(express.static(path.join(__dirname, 'public')));

// debug log user & session
app.use(logger(':date[clf] :method :url :status :response-time ms - :res[content-length]'));
if (app.get('env') === 'development') {
    // app.use(logger('dev'));

    //app.use(security.debugLog);
    app.set('view options', {
        pretty: true
    });
}

// CORS 부분 nginx 로 뺐으므로 에러 안 나면 제거할 것.2016.05.04
// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });

// Local Variables
// 모든 페이지에 user 객체를 넣어준다.
app.use(function(req, res, next) {
    res.locals.user = req.user;
    next();
});

// 루트를 제외한 모든 url에 대하여 로그인한 세션만 접근할 수 있도록 하였다.
app.use('/', root);
//app.use('/admin', authorization.ensureRequest.isPermitted("admin:view"), admin);
//app.use('/admin', security.isAdmin, admin);
app.use('/menu', security.isAuthenticated, menu);
// Chart View
app.use('/chart',security.isAuthenticated, chart);
// Chart RESTful Service
app.use('/rest', security.isAuthenticated,rest);
app.use('/rest',security.isAuthenticated, rest_biobank);
// File Upload
app.use('/files',security.isAuthenticated, files);

// Users Restful
app.use('/models/users',security.isAuthenticated, users);
// Sample
app.use('/models/sample',security.isAuthenticated, sample);
// cohort
app.use('/models/cohort',security.isAuthenticated, cohort);
// Drug Restful
app.use('/models/drug',security.isAuthenticated, drug);

// catch 404 and forward to error handler
//여기까지 왔다는 말은 처리할 핸들러가 없다는 뜻.
app.use(function(req, res, next) {
    var err = new Error('Page Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            user: req.user, // For Menu whether login, logout
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        user: req.user, // For Menu whether login, logout
        message: err.message,
        error: {}
    });
});

module.exports = app;
