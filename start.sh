#/usr/bin/bash
# 이 스크립트는 /etc/rc.local 파일에 등록되어 부팅 시 자동 실행된다.
# rc.local 이 자동 실행되기 위하여 chmod +x rc.local로 실행 옵션을 줘야 한다.
cd /data/public/node/ercsbweb;
pwd;
#PORT=3000 forever start  --minUptime 1000 --spinSleepTime 1000 \
#--uid "cgis" -l "/tmp/cgis.log" --append \
#--watch --watchDirectory "routes" bin/cluster.js
#pm2 stop all --watch 0
pm2 restart pm2.config.json
