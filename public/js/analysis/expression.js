(function ()	{
	'use strict';

	var sidebar = document.querySelector('#sidebar')
												.getBoundingClientRect(),
			navibar = document.querySelector('.navbar')
												.getBoundingClientRect(),
			target = document.querySelector('#main');

	var width = navibar.width - (sidebar.width + (sidebar.width / 5)) < 1620 ? 1620 : 
							navibar.width - (sidebar.width + (sidebar.width / 5)),
			height = sidebar.height - (navibar.bottom * 2) < 888.8 ? 888.8 : 
							sidebar.height - (navibar.bottom * 2);

	document.querySelector('#maincontent').style.height = 
	(sidebar.height - navibar.bottom * 1.2) + 'px';
	document.querySelector('.container').style.height = 
	(sidebar.height - navibar.bottom * 1.2) + 'px';

	target.style.width = width + 'px';
	target.style.height = height + 'px';

	$.ajax({
		type: 'GET',
		url: expressionUrl,
		data: expressionReqParams,
		beforeSend: function ()	{
			bio.loading().start(document.querySelector('#main'), width, height);
		},
		success: function (d)	{
			bio.expression({
				element: '#main',
				width: width,
				height: 800,
				requestData: expressionReqParams,
				data: d.data,
			});

			bio.loading().end();
		},
	});
}());