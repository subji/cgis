'use strict';

(function (window)	{
	var sidebar = document.querySelector('#sidebar'),
			navbar = document.querySelector('.navbar'),
			container = document.querySelector('.container');

	var sbcr = sidebar.getBoundingClientRect(),
			nbcr = navbar.getBoundingClientRect();

	container.style.width = (nbcr.width - sbcr.width * 1.3) + 'px';
	container.style.height = (sbcr.height - nbcr.height * 1.8) + 'px';
	container.style.margin = '0px';

	document.querySelector('#maincontent').style.marginBottom = '0px';

	var width = parseFloat(container.style.width),
			height = parseFloat(container.style.height);

	$.ajax({
		type: 'GET',
		url: pathwayURL,
		data: pathwayReqParams,
		beforeSend: function ()	{
			bio.loading().start(document.querySelector('#main'), width, height);
		},
		success: function(d)	{
			bio.pathway({
				width: width,
				height: height,
				element: '#main',
				data: {
					pathway: d.data.pathway_list,
					patient: d.data.gene_list,
					drugs: d.data.drug_list,
				},
				cancer_type: d.data.cancer_type,
			});

			bio.loading().end();
		},
	});
} (window));