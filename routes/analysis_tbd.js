var express = require('express');
var passport = require('passport');
var util = require('./modules/util');
var getConnection = require('./modules/mysql_connection');
var router = express.Router();

// menu에 대한 middleware : menu로 이동.
// router.use(function(req, res, next) {
//   console.log('middleware in analysis');
//   res.locals.sample = req.session.sample;
//   next();
// });

router.get('/first', function(req, res, next) {
  // 사용자가 화면 주소를 복사하여 다른 브라우저를 열고 붙여넣기했을 때 이 값이 없이 들어올 수 있다.
  // if (!res.locals.sample.id || !res.locals.sample.type)
  //   res.redirect('/menu');

  var sample_id = req.query.sample_id;

  if (util.isUndefined(sample_id))
    return res.redirect('/menu');

  // init sample session
  req.session.sample = {};

  // Get Sample Basic Info for Session.
  getConnection(function(connection) {
    connection.query('CALL CGIS.sp_getSampleBasic(?)', [sample_id], function(err, rows) {
      if (err)
        return next(err);
      req.session.sample = rows[0][0];
      req.session.sample.cohort_list = rows[1];

      res.redirect('summary');
    });
  });
});

router.get('/summary', function(req, res, next) {
  //req.session.sample.source 은 cohort를 저장하면 /models/cohort/filter 에서 사용자가 지정한 소스로 저장한다.
  //없으면 Default 는 GDAC

  var source = req.session.sample.source || 'GDAC';
  var cancer_type = req.session.sample.cancer_type;
  var sample_id = req.session.sample.sample_id;

  if (util.isUndefined(source, cancer_type, sample_id))
    return res.status(400).send('Not sufficient paramerters');

  var patient = {
    clinical: {},
    categories: {},
    genomic_alt: 0,
    mutational_cd: 0,
    cosmic_cgc: 0,
    fda_cancer: 0,
    fda_other_cancer: 0,
    clinical_trials: 0,
    implications: []
  };

  getConnection(function(connection) {
    connection.query('CALL CGIS.sp_getSampleSummary(?,?,?)', [source, cancer_type, sample_id], function(err, rows) {
      if (err)
        return next(err);
      patient.clinical = rows[0][0];
      patient.categories = rows[1];
      var list = rows[2];
      patient.genomic_alt = list.length;
      list.forEach(function(item) {
        if (item.mdAnderson > 0)
          patient.mutational_cd++;
        if (item.countOfCOSMIC > 0)
          patient.cosmic_cgc++;
        if (item.fda_cancer !== null)
          patient.fda_cancer += item.fda_cancer.split(',').length;
        if (item.fda_other_cancer !== null)
          patient.fda_other_cancer += item.fda_other_cancer.split(',').length;
        if (item.clinical_trials !== null)
          patient.clinical_trials += item.clinical_trials.split(',').length;
      });
      patient.implications = list;
      res.render('menu/analysis/summary', patient);
    });
  });
});

router.get('/globalsetting', function(req, res, next) {
  var source = req.query.source;
  var cancer_type = req.session.sample.cancer_type;

  if (util.isUndefined(source, cancer_type))
    return res.status(400).send('Not sufficient paramerters');

  getConnection(function(connection) {
    connection.query('CALL CGIS.sp_getCohortSubType(?,?)', [source, cancer_type], function(err, rows) {
      if (err)
        return next(err);

      // console.log('The solution is: ', rows);
      var subtypelist = rows[0];
      var items = rows[1];
      // subtype list에 해당 subtype 을 list에 push한다.
      items.forEach(function(item) {
        subtypelist.forEach(function(subtype) {
          if (subtype.subtype === item.subtype) {
            if (!subtype.list)
              subtype.list = [];
            subtype.list.push(item);
          }
        });
      });
      // console.log(subtypelist);
      res.render('menu/analysis/globalsetting', {
        subtypelist: subtypelist
      });
    });
  });
});

// Analysis는 샘플 아이디를 달고 다녀야 한다.
router.get('/:page', function(req, res, next) {
  var loc = 'menu/analysis' + '/' + req.params.page;
  res.render(loc);
});

module.exports = router;
