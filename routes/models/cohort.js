var express = require('express');
var util = require('../modules/util');
var getConnection = require('../modules/mysql_connection');
var router = express.Router();

router.get('/', function(req, res, next) {
  var source = req.query.source;
  var cancer_type = req.query.cancer_type;

  if (util.isUndefined(source, cancer_type))
    return res.status(400).send('Not sufficient paramerters');

  getConnection(function(connection) {
    connection.query('select CGIS.sf_cntOfCohortByCancerType(?,?) cnt', [source, cancer_type], function(err, rows) {
      if (err) return next(err);
      res.json(rows[0].cnt);
    });
  });

});

router.get('/filter', function(req, res, next) {
  var source = req.query.source;
  var cancer_type = req.query.cancer_type;
  var filter_option = req.query.filter_option;

  if (util.isUndefined(source, cancer_type, filter_option))
    return res.status(400).send('Not sufficient paramerters');

  getConnection(function(connection) {
    connection.query('CALL CGIS.sp_getCountOfCohortByFilter(?,?,?)', [source, cancer_type, filter_option], function(err, rows) {
      if (err) return next(err);
      // console.log(rows[0]);
      // 이값을 session에 저장하여 이용할 수 있도록 한다.
      req.session.sample.source = source;
      req.session.sample.filter_option = filter_option;

      res.json(rows[0][0]);
    });
  });

});

module.exports = router;
